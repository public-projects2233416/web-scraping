#This will not run on online IDE
import requests
import json
from bs4 import BeautifulSoup

site = "https://www.rightmove.co.uk"
minBedroom = 2
minPrice = 200000
maxPrice = 100000
distance = 0.5
URL = f"{site}/property-for-sale/find.html?locationIdentifier=POSTCODE%5E4393686&minBedrooms={minBedroom}&maxPrice={maxPrice}&minPrice={minPrice}&radius={distance}&propertyTypes=detached%2Csemi-detached%2Cterraced&includeSSTC=false&mustHave=&dontShow=&furnishTypes=&keywords="


def store_data(id, address, price, updatedDate):
    data = {}
    data['id'] = id
    data['address'] = address
    data['price'] = price
    data['date'] = updatedDate
    json_data = json.dumps(data)
    print(json_data)
    resp = json.loads(json_data)
    print(resp['price'])    


def normalise_address(data):
    return data.split('-')[0]


def get_houses_basic():
    savedIds = ['']
    r = requests.get(URL,
                     headers={
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-GB,en;q=0.9",
        "Connection": "keep-alive",
        "Host": "www.rightmove.co.uk",
        "Sec-Fetch-Dest": "document",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-Site": "none",
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.5 Safari/605.1.15})"
        }
    )
    soup = BeautifulSoup(r.content, 'html5lib') # If this line causes an error, run 'pip install html5lib' or install html5lib
    for a in soup.find_all('div', attrs = {'class': 'propertyCard'}):
        id = a.find('a', attrs = {'class': 'propertyCard-anchor'})['id'].replace('prop', '')
        if id not in savedIds:
            price = a.find('div', attrs = {'class': 'propertyCard-priceValue'}).text.strip()
            addressTag = a.find('address', attrs = {'class': 'propertyCard-address'})
            address = normalise_address(addressTag.find('span').text.strip())
            updatedDate = a.find('span', attrs = {'class': 'propertyCard-branchSummary-addedOrReduced'}).text.strip()[-10:]

            if price != "" and updatedDate != "" and address != "":
                store_data(id, address, price, updatedDate)


def main():
    get_houses_basic()    


if __name__ == "__main__":
    main()
